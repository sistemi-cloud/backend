module.exports = {
  plugins: [
    '@semantic-release/commit-analyzer',
    '@semantic-release/release-notes-generator',
    '@semantic-release/npm',
    '@semantic-release/git',
    [
      '@semantic-release/gitlab',
      {
        gitlabUrl: 'https://gitlab.com',
        repositoryUrl: 'https://gitlab.com/sistemi-cloud/backend'
      }
    ]
  ]
}
