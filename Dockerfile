FROM node:16

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app
COPY ./src /usr/src/app/src

RUN npm install --omit=dev
COPY .babelrc /usr/src/app
COPY .eslintrc /usr/src/app
# COPY .env /usr/src/app/
COPY .env.example /usr/src/app/
ENV NODE_ENV production

ENV PORT 8080
EXPOSE 8080

CMD ["npm", "start"]
