/* eslint-disable no-unused-vars */
import path from 'path'
import merge from 'lodash/merge'

/* istanbul ignore next */
const requireProcessEnv = (name) => {
  if (!process.env[name] && process.env.NODE_ENV === 'production') {
    throw new Error('You must set the ' + name + ' environment variable')
  }
  return process.env[name]
}

/* istanbul ignore next */
/*
if (process.env.NODE_ENV !== 'production') {
  const dotenv = require('dotenv-safe')
  dotenv.config({
    path: path.join(__dirname, '../.env'),
    example: path.join(__dirname, '../.env.example')
  })
}
*/

const config = {
  all: {
    env: process.env.NODE_ENV || 'development',
    root: path.join(__dirname, '..'),
    port: process.env.PORT || 9000,
    ip: process.env.IP || '0.0.0.0',
    apiRoot: process.env.API_ROOT || '',
    masterKey: requireProcessEnv('MASTER_KEY'),
    jwtSecret: requireProcessEnv('JWT_SECRET'),
    mongo: {
      options: {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true
      }
    }
  },
  test: { },
  development: {
    ip: process.env.IP || undefined,
    port: process.env.PORT || 9000,
    mongo: {
      uri: 'mongodb://localhost/backend-dev',
      options: {
        debug: true
      }
    },
    log: {
      info: process.env.ENABLE_LOG_INFO || true,
      warn: process.env.ENABLE_LOG_WARN || true,
      error: process.env.ENABLE_LOG_ERROR || true,
      debug: process.env.ENABLE_LOG_DEBUG || true
    }
  },
  production: {
    ip: process.env.IP || undefined,
    port: process.env.PORT || 8080,
    mongo: {
      uri: process.env.MONGODB_URI || 'mongodb://localhost/backend'
    },
    log: {
      info: process.env.ENABLE_LOG_INFO || true,
      warn: process.env.ENABLE_LOG_WARN || true,
      error: process.env.ENABLE_LOG_ERROR || true,
      debug: process.env.ENABLE_LOG_DEBUG || true
    }
  }
}

module.exports = merge(config.all, config[config.all.env])
export default module.exports
