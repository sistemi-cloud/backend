import { Router } from 'express'
import { login, register } from './controller'
import { password as passportPwd, master, basic, token, revoke } from '../../services/passport'
import { middleware as body } from 'bodymen'
import { schema as userSchema } from '../user/model'
import { create } from '../user/controller'

const { email, password, name, picture, role } = userSchema.tree

const router = new Router()
/**
 * @api {post} /auth/register Registration
 * @apiName Registration
 * @apiGroup Auth
 * @apiPermission public
 * @apiParam {String} access_token Master access_token.
 * @apiParam {String} email User's email.
 * @apiParam {String{6..}} password User's password.
 * @apiParam {String} [name] User's name.
 * @apiParam {String} [picture] User's picture.
 * @apiParam {String=user,admin} [role=user] User's role.
 * @apiSuccess (Success 201) {Object} user User's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 409 Email already registered.
 */

router.post('/register',
  body({ email, password, name, picture }),
  register)

/**
 * @api {post} /auth Authenticate
 * @apiName Authenticate
 * @apiGroup Auth
 * @apiPermission master
 * @apiHeader {String} Authorization Basic authorization with email and password.
 * @apiParam {String} access_token Master access_token.
 * @apiSuccess (Success 201) {String} token User `access_token` to be passed to other requests.
 * @apiSuccess (Success 201) {Object} user Current user's data.
 * @apiError 401 Master access only or invalid credentials.
 */
router.post('/login',
  // master(),
  passportPwd(),
  // basic(),
  login)

/**
 * @api {post} /auth/logout Logout
 * @apiName Authenticate
 * @apiGroup Auth
 * @apiPermission private
 * @apiHeader {String} access_token user access token.
 * @apiSuccess (Success 200) No content.
 * @apiError 401 Not authorized.
 */
router.post('/logout',
  token({ required: true }),
  revoke())

export default router
