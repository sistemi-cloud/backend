import { sign } from '../../services/jwt'
import { success } from '../../services/response/'
import { User } from '../user'
import Logger from '../../services/logger'

export const register = ({ bodymen: { body } }, res, next) => {
  const logger = Logger('Auth:SignUp')
  logger.debug('Request BODY: ', body)
  User.create({
    ...body,
    role: 'user',
    picture: 'https://gravatar.com/avatar/50e5fc5eea9f7b7a80d0174aaf02c4d1?d=identicon'
  })
    .then(user => {
      sign(user.id)
        .then((token) => ({
          token,
          user: user.view(true)
        }))
        .then(success(res, 201))
    })
    .catch((err) => {
      /* istanbul ignore else */
      if (err.name === 'MongoError' && err.code === 11000) {
        res.status(409).json({
          valid: false,
          param: 'email',
          message: 'email already registered'
        })
      } else {
        next(err)
      }
    })
}

export const login = ({ user }, res, next) => {
  return sign(user.id)
    .then((token) => ({ token, user: user.view(true) }))
    .then(success(res, 201))
    .catch(next)
}
