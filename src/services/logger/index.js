import config from '../../config'
const backendLogger = require('debug')('Backend')
const { log } = config
const LOG_INFO = log.info
const LOG_WARN = log.warn
const LOG_ERROR = log.error
const LOG_DEBUG = log.debug

const logger = (entity) => ({

  /**
   * info logging level
   * @param params params to log at info level
   *
   */
  info: (...params) => {
    if (LOG_INFO) {
      backendLogger('\x1b[36m', `${new Date().toISOString()} - INFO - ${entity}: `, ...params)
    }
  },
  /**
   * warning logging level
   * @param params params to log at info level
   *
   */
  warn: (...params) => {
    if (LOG_WARN) {
      backendLogger('\x1b[33m', `${new Date().toISOString()} - WARN - ${entity}: `, ...params)
    }
  },
  /**
   * error logging level
   * @param params params to log at info level
   *
   */
  error: (...params) => {
    if (LOG_ERROR) {
      backendLogger('\x1b[31m', `${new Date().toISOString()} - ERROR - ${entity}: `, ...params)
    }
  },
  /**
   * debug logging level
   * @param params params to log at info level
   *
   */
  debug: (...params) => {
    if (LOG_DEBUG) {
      backendLogger('\x1b[35m', `${new Date().toISOString()} - DEBUG - ${entity}: `, ...params)
    }
  },
  /**
   * info logging level for promise
   * @param controller controller name to log at info level
   * @param params params to log at info level
   *
   */
  infoPromise: (controller) => (...params) => {
    if (LOG_INFO) {
      backendLogger('\x1b[36m', `${new Date().toISOString()} - INFO - ${entity} - ${controller}: `, ...params)
    }
    if (entity) {
      return entity
    } else return null
  },
  /**
   * warning logging level for promise
   * @param controller controller name to log at warning level
   * @param params params to log at info level
   *
   */
  warnPromise: (controller) => (...params) => {
    if (LOG_WARN) {
      backendLogger('\x1b[33m', `${new Date().toISOString()} - WARN - ${entity} - ${controller}: `, ...params)
    }
    if (entity) {
      return entity
    } else return null
  },
  /**
   * error logging level for promise
   * @param controller controller name to log at error level
   * @param params params to log at info level
   *
   */
  errorPromise: (controller) => (...params) => {
    if (LOG_ERROR) {
      backendLogger('\x1b[31m', `ERROR - ${entity} - ${controller}:`, ...params)
    }

    if (entity) {
      return entity
    } else return null
  },
  /**
   * debug logging level
   * @param controller controller name to log at debug level
   * @param params params to log at info level
   *
   */
  debugPromise: (controller) => (...params) => {
    if (LOG_DEBUG) {
      backendLogger('\x1b[35m', `DEBUG - ${entity} - ${controller}:`, ...params)
    }
    if (entity) {
      return entity
    } else return null
  }
})

const _logger = logger('Logger')
_logger.info('Log info level enabled')
_logger.warn('Log warning level enabled')
_logger.error('Log error level enabled')
_logger.debug('Log debug level enabled')

export default logger
