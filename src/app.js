import http from 'http'
import { env, mongo, port, ip, apiRoot } from './config'
import mongoose from './services/mongoose'
import express from './services/express'
import api from './api'
import Logger from './services/logger'

const app = express(apiRoot, api)
const server = http.createServer(app)

if (mongo.uri) {
  mongoose.connect(mongo.uri)
}
mongoose.Promise = Promise

const logger = Logger('Main')

setImmediate(() => {
  server.listen(port, ip, () => {
    logger.info('Express server listening on http://%s:%d, in %s mode', ip, port, env)
  })
})

export default app
